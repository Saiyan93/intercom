require_relative '../../lib/flattify'

describe Array, "flattify method" do
  context "with empty Array" do
    it "returns empty array without failure" do
      expect([].flattify).to eq([])
    end
  end

  context "with Array containing multiple nested arrays" do
    it "returns flattened Array" do
      expect([1, [2, 1, [5, [5, [1, [2]]]], 0], 17].flattify). to eq([1, 2, 1, 5, 5, 1, 2, 0, 17])
    end
  end

  context "with Array containing nil elements" do
    it 'returns flattened Array with nil elements in their respective positions' do
      expect([1, [2, nil, [5, [nil]], nil], 17].flattify).to eq([1, 2, nil, 5, nil, nil, 17])
    end

    it "returns unchanged array if Array has single nil element" do
      expect([nil].flattify).to eq([nil])
    end
  end

end
