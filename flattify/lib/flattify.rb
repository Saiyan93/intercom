#!/usr/bin/env ruby

class Array

  # Add flattify method to Array class. It behaves like flatten without
  # level parameter option as it is supposed to flatten till an unflattened array
  # is achieved. It can be used like:
  #
  # [[1]].flattify (preferred)
  # => [1]
  #
  # Array.new.flattify([[1]])
  # => [1]
  #
  # Input: {Array}
  # Output: {Array}
  #
  def flattify(nested_array = self)

    flat_array = []

    nested_array.each do |element|
      if element.is_a?(Array)
        flat_array.concat(flattify(element))
      else
        flat_array << element
      end
    end

    flat_array
  end

end
