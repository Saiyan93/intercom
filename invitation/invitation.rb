#!/usr/bin/env ruby

require_relative './lib/invitation'

if $PROGRAM_NAME == __FILE__
  invitation = Invitation.new
  invitation.invite_within_proximity(data_path: "./lib/data/customers.json")
end
