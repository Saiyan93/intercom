require_relative '../../../lib/algorithms/great_circle_distance'

describe GreatCircleDistance, "Constructor tests" do
  context "when coordinates are passed to the algorithm" do
    it "converts the coordinate degrees to radians" do

      great_circle_distance = GreatCircleDistance::Processor.new(destination_lat: "52.986375",
                                                                 destination_long: "-6.043701",
                                                                 origin_lat: 53.3381985,
                                                                 origin_long: -6.2592576)

      expect(great_circle_distance.destination_lat_radians).to eq(0.9247867024464104)
      expect(great_circle_distance.destination_long_radians).to eq(-0.10548248145607382)
      expect(great_circle_distance.origin_lat_radians).to eq(0.9309271809073008)
      expect(great_circle_distance.origin_long_radians).to eq(-0.10924465385047823)
    end
  end

  context "#get_arc_length calculates distance accurately" do
    it "returns correct arc length" do
      great_circle_distance = GreatCircleDistance::Processor.new(destination_lat: "52.986375",
                                                                 destination_long: "-6.043701",
                                                                 origin_lat: 53.3381985,
                                                                 origin_long: -6.2592576)
      expect(great_circle_distance.arc_length).to eq(41.68)
    end
  end
end
