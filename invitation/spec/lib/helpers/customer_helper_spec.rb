require_relative '../../../lib/helpers/customer_helper'

describe CustomerHelper, "Filter Class tests" do
  context "#get_people_in_proximity with keyword arguments" do
    it "raises ArgumentError" do
      expect { CustomerHelper::Filter.get_people_in_proximity }.to raise_error(ArgumentError)
    end

    it "does not raise ArgumentError when passes all required fields" do
      expect {
        CustomerHelper::Filter.get_people_in_proximity(distance: 50,
                                                       latitude: 50.2,
                                                       longitude: -6.3,
                                                       data_path: "./spec/lib/fixtures/customers_fixture.json")
        }.to_not raise_error
    end

    it "returns customers which are only within range" do
      customers = CustomerHelper::Filter.get_people_in_proximity(distance: 100,
                                                                 latitude: 53.3381985,
                                                                 longitude: -6.2592576,
                                                                 data_path: "./spec/lib/fixtures/customers_fixture.json")

      expect(customers.count).to eq(3)
    end
  end

  context "#within_proximity? with keyword arguments" do
    it "returns false for coordinates of customer when they are out of distance-range" do
      proximity_bool = CustomerHelper::Filter.within_proximity?(distance: 100,
                                                                customer_lat: 52.240382,
                                                                customer_long: -6.972413,
                                                                base_lat: 53.3381985,
                                                                base_long: -6.2592576)

      expect(proximity_bool).to be_falsy
    end

    it "returns true for coordinates of customer when they within distance-range" do
      proximity_bool = CustomerHelper::Filter.within_proximity?(distance: 100,
                                                                customer_lat: 54.0894797,
                                                                customer_long: -6.18671,
                                                                base_lat: 53.3381985,
                                                                base_long: -6.2592576)

      expect(proximity_bool).to be_truthy
    end
  end
end
