require_relative '../lib/invitation'

describe Invitation, "constructor tests" do
  context "without latitude and longitude" do
    it "instantiates class with Intercom's coordinates" do
      invitation = Invitation.new

      expect(invitation.base_latitude).to eq(53.3381985)
      expect(invitation.base_longitude).to eq(-6.2592576)
    end
  end

  context "with latitude and longitude" do
    it "instantiates class with Intercom's coordinates" do
      invitation = Invitation.new(latitude: 15.23, longitude: -23.43)

      expect(invitation.base_latitude).to eq(15.23)
      expect(invitation.base_longitude).to eq(-23.43)
    end
  end
end

describe Invitation, "integration tests" do
  context "with fixtures file as data" do
    it "instantiates class with Intercom's coordinates" do
      invitation = Invitation.new

      expected_output = "Name: Eoin Ahearn, ID: 8\n" \
                        "Name: Christina McArdle, ID: 12\n" \
                        "Name: Stephen McArdle, ID: 26\n"

      expect {
        invitation.invite_within_proximity(data_path: "./spec/lib/fixtures/customers_fixture.json")
      }.to output(expected_output).to_stdout
    end
  end
end
