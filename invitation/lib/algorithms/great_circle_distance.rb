#!/usr/bin/env ruby

module GreatCircleDistance
  class Processor

    attr_reader :destination_lat_radians, :destination_long_radians,
                :origin_lat_radians, :origin_long_radians

    #  We convert latitude and longitudinal coordinate to radius using
    #  `convert_to_radians` method. Great Circle Distance uses radians to
    #  calculate shortest distance.
    #
    #  Input: {destination_lat: Integer/Float (required),
    #          destination_long: Integer/Float (required),
    #          origin_lat: Integer Float (required),
    #          origin_long: Integer/Float (required)}
    # Output: {Processor Instance} with coordinates converted to radians
    #
    def initialize(destination_lat: 0.0, destination_long: 0.0, origin_lat: 0.0, origin_long: 0.0)
      @destination_lat_radians = convert_to_radians(destination_lat)
      @destination_long_radians = convert_to_radians(destination_long)
      @origin_lat_radians = convert_to_radians(origin_lat)
      @origin_long_radians = convert_to_radians(origin_long)
    end

    RADIUS_OF_EARTH = 6371.0

    #  Great Circle Distance or orthodromic distance is the shortest distance
    #  between two points on the surface of a sphere. We calculate the distance
    #  distance between one coordinate to another on Earth by multiplying central
    #  angle with radius of Earth.
    #
    #  Algorithm Wiki => https://en.wikipedia.org/wiki/Great-circle_distance
    #
    #  Input: None
    #  Output: {Float} rounded to 2 decimal places.
    def arc_length
      central_angle = Math.acos(cosine_mark)
      (RADIUS_OF_EARTH * central_angle).round(2)
    end

    private

      def cosine_mark
        longitudinal_delta = (destination_long_radians - origin_long_radians).abs
        ( Math.sin(destination_lat_radians) * Math.sin(origin_lat_radians) ) + \
        ( Math.cos(destination_lat_radians) * Math.cos(origin_lat_radians) * Math.cos(longitudinal_delta) )
      end

      def convert_to_radians(value)
        value.to_f * Math::PI / 180
      end
  end

end
