#!/usr/bin/env ruby

require_relative './helpers/customer_helper'

class Invitation

  attr_reader :base_latitude, :base_longitude

  #  Constructor method with default values of base latitude and longitude
  #  set to Intercom's location in the problem statement. attr_reader helps
  #  in displaying the coordinates of Invitation methods, gives clear visibility
  #  of this abstraction. For eg:
  #
  #  invitation = Invitation.new
  #  invitation.base_latitude
  #  => 53.3381985
  #  inviation.base_longitude
  #  => -6.2592576
  #
  #  Input: {Integer, Integer}
  #  Output: {Invitation object}
  #
  def initialize(latitude: 53.3381985, longitude: -6.2592576)
    @base_latitude = latitude
    @base_longitude = longitude
  end

  # `invite_within_proximity` takes in distance parameter and is set to 100 Km
  #  by default, as the problem statement requires people within 100 km.
  #  It injests data and filters people with the help of CustomerHelper::Filter
  #  helper and outputs people within specified distance/proximity.
  #
  #  It then sorts customers according to "user_id" attribute of customer data,
  #  and then prints user to STDOUT
  #
  #  Input: {String, Integer/Float}
  #  returns output of `display_customers`
  #
  def invite_within_proximity(data_path:, distance: 100)
    customers_in_proximity = CustomerHelper::Filter.get_people_in_proximity(distance: distance,
                                                                            latitude: base_latitude,
                                                                            longitude: base_longitude,
                                                                            data_path: data_path)
    customer_sorted_by_id =  customers_in_proximity.sort_by { |customer| customer["user_id"] }
    display_customers(customer_sorted_by_id)
  end

  private

    #  Input: {Array of customer data with atleast `name` and `user_id` attributes}
    #  Output: {Print to STDOUT `name` and `user_id` of each customers}
    #
    def display_customers(customers = [])
      customers.each do |customer|
        puts "Name: #{customer['name']}, ID: #{customer['user_id']}"
      end
    end

end
