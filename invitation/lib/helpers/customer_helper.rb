#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require 'byebug'
require_relative '../algorithms/great_circle_distance'

module CustomerHelper
  class Filter

    class << self

      #  `get_people_in_proximity` takes in distance-range, base_latitude and base_longitude
      #  as parameters. The purpose of this method is to ingest data from File and add
      #  customer details to customers array "only if" they are within proximity. We do not
      #  want to load all customer details from file onto memory if they dont fit withing
      #  the range.
      #
      #  Input: {distance: Integer/Float (required), latitude: Integer/Float (required),
      #          longitude: Integer/Float (required)}
      #  Output: {Array of customers within distance-range}
      #
      def get_people_in_proximity(distance:, latitude:, longitude:, data_path:)
        customers = []
        file = File.open(data_path)
        file.each_line do |line|
          customer_details = JSON.parse(line)
          customers << customer_details if within_proximity?(distance: distance,
                                                             customer_lat: customer_details["latitude"],
                                                             customer_long: customer_details["longitude"],
                                                             base_lat: latitude,
                                                             base_long: longitude)
        end
        file.close    # Always close file explicitly if File.open is not used with a block.
        customers
      end

      #  `within_proximity?` takes in distance-range, customer_latitude, customer_longitude,
      #  base_latitude and base_longitude as parameters. The purpose of this method is to
      #  extrapolate great circle distance using GreatCircleDistance algorithm module and
      #  return Boolean indicating if the customer's coordinates fall within distance range.
      #
      #  Input: {distance: Integer/Float (required), customer_lat: Integer/Float (required),
      #          customer_long: Integer/Float (required), base_lat: Integer/Float,
      #          base_long: Integer/Float}
      #  Output: {Boolean}
      #
      def within_proximity?(distance:, customer_lat:, customer_long:, base_lat: 0.0, base_long: 0.0)
        great_circle_distance = GreatCircleDistance::Processor.new(destination_lat: customer_lat,
                                                                   destination_long: customer_long,
                                                                   origin_lat: base_lat,
                                                                   origin_long: base_long)
        great_circle_distance.arc_length <= distance
      end

    end

  end
end
